import React from 'react';
import Profile from "../components/Profile";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import {withAuthRedirect} from "../hoc/withAuthRedirect";
import {getUserProfile, getUserStatus, updateProfile, updateStatus, uploadAvatarImage} from "../thunks";

class ProfileContainer extends React.Component {

    refreshProfile(){
        let userId = this.props.match.params.userId;
        if (!userId) {
            userId = this.props.authorizedUserId;
            if(!userId)
                this.props.history.push('/login');
        }
        this.props.getUserProfile(userId);
        this.props.getUserStatus(userId);
    }

    componentDidMount() {
        this.refreshProfile();
    }

    componentDidUpdate (prevProps){
        if(this.props.match.params.userId != prevProps.match.params.userId) {
            this.refreshProfile();
        }
    }

    render() {
        return (
            <Profile profile={this.props.profile}
                     // isOwner ={!this.props.match.params.userId}
                     status={this.props.status}
                     uploadAvatarImage ={this.props.uploadAvatarImage}
                     updateStatus={this.props.updateStatus}
                     updateProfile={this.props.updateProfile}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile.profile,
        authorizedUserId: state.auth.userId,
        status: state.profile.status,
        isAuth: state.auth.isAuth
    }
};

export default compose(withRouter, (connect(mapStateToProps,
    {
        getUserProfile,
        getUserStatus,
        updateStatus,
        uploadAvatarImage,
        updateProfile
    })))(ProfileContainer);