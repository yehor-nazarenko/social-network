import React from 'react';
import {NavLink} from 'react-router-dom';

const Dialog = (props) => {
    return (
        <NavLink to={'/dialog/' + props.id} activeClassName='active'>{props.name}</NavLink>
    );
};

export default Dialog;