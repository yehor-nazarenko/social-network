import React from "react";
import Dialog from "./Dialog";
import Message from "./Message";
import {Field, reduxForm} from "redux-form";
import {Textarea} from "./commons/FormControls";
import {maxLength30, required} from "../helpers/validators";

const Messages = ({dialogs, messages, addMessage}) => {
    const dialogElements =
        dialogs.map(d => <Dialog id={d.id} key={d.id} name={d.name}/>);

    const messageElements =
        messages.map(m => <Message id={m.id} key={m.id} message={m.message}/>);

    const addNewMessage = (e) => {
        addMessage(e.newMessageText);
    };

    return (
        <div>
            <div>
                {dialogElements}
            </div>
            <div>
                {messageElements}
            </div>

            <AddMessageForm onSubmit={addNewMessage}/>
        </div>
    )
};

const MessagesForm = (props) => {
    return   <form onSubmit={props.handleSubmit}>
        <Field component={Textarea}
               name='newMessageText'
               placeholder='Enter your message'
               validate={[required, maxLength30]}
        />
        <button>add</button>
    </form>
};

export const AddMessageForm = reduxForm({form: "dialogAddMessageForm"})(MessagesForm);

export default Messages;