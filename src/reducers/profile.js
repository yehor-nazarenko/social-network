import {ADD_POST, SET_USER_PHOTO, SET_USER_PROFILE, SET_USER_STATUS} from "../consts"


let initialState = {
    posts: [
        {id: 1, post: 'My first post', likesCount: 12},
        {id: 2, post: 'Wow It\'s working', likesCount: 3},
        {id: 3, post: 'Bla bla bla', likesCount: 7}
    ],
    profile: null,
    status: ''
};

const profile = (state = initialState, action) => {

    switch (action.type) {

        case ADD_POST:
            return {
                ...state,
                posts: [...state.posts, {id: 5, post: action.payload, likesCount: 0}]
            };

        case SET_USER_PHOTO:
            return {
                ...state,
                profile: {...state.profile, photos: action.photos}
            };

        case SET_USER_PROFILE:
            return {
                ...state,
                profile: action.profile
            };

        case SET_USER_STATUS:
            return {
                ...state,
                status: action.status
            };

        default:
            return state;
    }
};

export default profile;

