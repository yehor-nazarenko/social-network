import {authAPI} from "../api/api";
import {setCaptcha, setUserAuthData} from "../actions";
import {stopSubmit} from "redux-form";

export const setAuthData = () => async (dispatch) => {
    const data = await authAPI.getAuth();
    if (data.resultCode === 0) {
        let {id, login, email} = data.data;
        dispatch(setUserAuthData(id, login, email, true));
    }
};

export const login = (email, password, rememberMe, captcha) => async (dispatch) => {
    const data = await authAPI.login(email, password, rememberMe, captcha);
    if (data.resultCode === 0) {
        dispatch(setAuthData());
    } else {
        if (data.resultCode === 10) dispatch(getCaptcha());
        let message = data.messages.length > 0 ? data.messages[0] : "Server Error";
        dispatch(stopSubmit("login", {_error: message}));
    }
};

export const logout = () => async (dispatch) => {
    const data = await authAPI.logout();
    if (data.resultCode === 0) {
        dispatch(setUserAuthData(null, null, null, false));
    }
};

export const getCaptcha = () => async (dispatch) => {
    const data = await authAPI.captcha();
    dispatch(setCaptcha(data.url));
};