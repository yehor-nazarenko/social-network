import {
    SET_CAPTCHA,
    SET_USER_DATA
} from "../consts";


export const setUserAuthData = (userId, login, email, isAuth) => ({
    type: SET_USER_DATA,
    payload: {userId, login, email, isAuth}
});

export const setCaptcha = (url) => ({
    type: SET_CAPTCHA,
    payload: url
});

